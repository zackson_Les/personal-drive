package in.Server;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

import in.checksum.checksumch;

public class SreverThered extends Thread{
	int a = 0;
	int check = 0;
	static Random rnd = new Random();
	String usrstatus = "";
	private final Socket clientsocket;
	private final Server server;
	private OutputStream output;
	private HashSet<String> topicset = new HashSet<>();
	String login; 
	public String getLogin() {
		return login;
	}
	public SreverThered(Server server, Socket clientsocket) {
		this.server = server;
		this.clientsocket = clientsocket;
	}
	@Override
	public void run() {
		try {
			handleclientSocket();

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void handleclientSocket() throws IOException, InterruptedException {
		this.output = clientsocket.getOutputStream();
		InputStream input = clientsocket.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line;
		int Startlogin = 0;
		int exit = 0;
		while((line = reader.readLine()) != null) {
			String [] tokens = line.split(" ");

			//System.out.println(checksumch.Check('t' + 'r' + 'u' + 'e'));
			while(Startlogin == 0) {
					
				if(checksumch.UnCheck(line)) {
					Startlogin = 1;
					break;
				}else{
					exit++;
					if(exit >= 1) {
						output.write("Error-no.".getBytes());
						System.err.println("Check ip: " + clientsocket.getInetAddress());
						while(true);
					}
				}
			}
			
			if(tokens != null && tokens.length > 0 && Startlogin == 1) {
				String cmd = tokens[0];
				//String status = usercheck.fina + " is " + usrstatus ;
				// output.write(status.getBytes());

				if("login".equals(cmd)) {
					//System.out.println(line);
					String userso = "";
					String num = tokens[1];

					if(num.equals("@rycb$%kdop%+lkadm34!!")) {
						login = "main";
						int chola = rnd.nextInt(21 - 1) + 1;
						char chalala = "@rycb$%kdop%+lkadm34!!".charAt(chola);
						userso = checksumch.Check(chalala);
					}

					if(num.equals("$#nja455Kj3456JAnAj33K") ) {
						login = "Harel";
						int chola = rnd.nextInt(21 - 1) + 1;
						char chalala = "$#nja455Kj3456JAnAj33K".charAt(chola);
						userso = checksumch.Check(chalala);
					}

					if(num.equals("z%M_E&CoD(o&CwI=>rI<f?")) {
						login = "Hajaj";
						int chola = rnd.nextInt(21 - 1) + 1;
						char chalala = "z%M_E&CoD(o&CwI=>rI<f?".charAt(chola);
						userso = checksumch.Check(chalala);
					}

					if(num.equals("notvalid.err")) {
						userso = "not";
					}

					//0235972044
					//System.out.println(line);
					System.out.println(login + " is conected.");

					//boolean baba = reader.readLine().equals("5330=?");
					//if(baba){
					System.out.println(login + " has loged in.");

					//String msg = "online " + login  + "\n";
					
					usrstatus = login + " " + "online\n";

					output.write((userso + "\n").getBytes());
					
					
					if("logoff".equalsIgnoreCase(cmd)) {
						check = 1;
					}

					//4034200071
					List<SreverThered> workerlist = server.getworkerslist();
					for(SreverThered server : workerlist) {
						if(!server.getLogin().equals(login)) {
							if(server.getLogin() != null) {
								String usrstatus2 = server.getLogin() + " " + "online\n";
									send(usrstatus2);
							}
						}
					}

					for(SreverThered server : workerlist) {
						if(!server.getLogin().equals(login)) {
							server.send(usrstatus);
						}
					}
				}

				else if("msg".equalsIgnoreCase(cmd)) {
					String[] tokensmsg = line.split(" ", 3);
					handleMassege(tokensmsg);
				}

				else if("join".equalsIgnoreCase(cmd)) {
					handleJoin(tokens);
				}
				
				else if("leave".equalsIgnoreCase(cmd)) {
					handleLeave(tokens);
				}

				else if("logoff".equalsIgnoreCase(cmd) || check == 1) {
					usrstatus = "offline";
					String msg = login + " " + usrstatus + "\n";
					System.out.println(login + " has disconected.");
					List<SreverThered> workerlist = server.getworkerslist();
					for(SreverThered server : workerlist) {
						server.send(msg);
					}
					System.out.println("Connection Ended by Client.");
					server.removeThread(this);
					clientsocket.close();
					break;
				}
			}
		}
	} 


	private void handleLeave(String[] tokens) throws IOException {
		if(tokens.length > 1) {
			String topic = tokens[1];
			topicset.remove(topic);
			output.write((login + " left " + topic + "\n").getBytes());
		}

	}

	public boolean ismemberoftopic(String topic) {
		return topicset.contains(topic);
	}

	private void handleJoin(String[] tokens) throws IOException {
		if(tokens.length > 1) {
			String topic = tokens[1];
			topicset.add(topic);
		}
	}

	private void handleMassege(String[] tokensmsg) throws IOException {
		String sendto = tokensmsg[1];
		String body = tokensmsg[2];

		boolean istopic = sendto.charAt(0) == '#';

		List<SreverThered> workerlist = server.getworkerslist();
		for(SreverThered server : workerlist) {
			if(istopic) {
				if(server.ismemberoftopic(sendto)){
					String outmsg = "msg " + login + " " + body + "\n";
					server.send(outmsg);
				}
			}else {
				if(sendto.equalsIgnoreCase(server.getLogin())) {
					String outmsg = "msg " + login + " " + body + "\n";
					server.send(outmsg);
				}
			}
		}
	}

	private void send(String msg) throws IOException {
		if(login != null) {
			output.write(msg.getBytes());
		}
	}
}

