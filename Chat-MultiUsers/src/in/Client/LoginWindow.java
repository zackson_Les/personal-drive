package in.Client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import in.usrcheck.usercheck;

public class LoginWindow extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3458369094580679002L;
	/**
	 * 
	 */
	JTextField LoginF = new JTextField();
	JTextField serveripF = new JTextField();
	//JTextField serverportF = new JTextField();
	JPasswordField passwordF = new JPasswordField();
	JButton loginbut = new JButton("Login");
	JButton logoutbut = new JButton("Logoff");
	JButton topicbut = new JButton("join topic");
	JTextArea area = new JTextArea();
	String topic;
	String a;
	private Clinet client;

	public LoginWindow() {
		super("Login");

		String ipnum = serveripF.getText();
		String portnum = "550";
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		 

		this.client = new Clinet(ipnum ,portnum);
		
		logoutbut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					client.logoff();{
						System.exit(0);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
			}
			
		});
		loginbut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doLogin();
			}
			private void doLogin() {
				usercheck.main(null);{
					String login = LoginF.getText();
					@SuppressWarnings("deprecation")
					String Password = passwordF.getText();
					if(login.equals(usercheck.fina) && Password.equals(usercheck.fona)){
						if(client.connect()) {
							client.Login();
						Usergui usergui = new Usergui(client);
						JFrame frame = new JFrame("User List");
						
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						
						frame.setSize(400, 600);
						frame.getContentPane().add(usergui, BorderLayout.CENTER);
						frame.add(logoutbut, BorderLayout.AFTER_LINE_ENDS);
						frame.setVisible(true);
						
						setVisible(false);
						}else {
							JOptionPane.showInputDialog(this, "Error - Login");
						}
					}
				}
			}
		});
		
		//0235972044
		
		JPanel p = new JPanel();
		area.setText("Connect the key and Enter User name and Password:");

		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(area);
		p.add(serveripF);
		p.add(LoginF);
		p.add(passwordF);
		p.add(loginbut);
		
		getContentPane().add(p, BorderLayout.CENTER);
		
		pack();
	
		setVisible(true);
		
	}
public static void main(String[] args) {
	LoginWindow window = new LoginWindow();
	window.setVisible(true);
	}
}

