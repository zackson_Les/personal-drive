package in.SERVERCLIENT;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

import drive.in.en.Base16Encoder;
import in.key.encryption.*;
import zero.in.*;

public class CLIENT {
	static Scanner in = new Scanner(System.in);

	
	public static void CLOONT(String serverip, int serversocket) throws UnknownHostException, IOException {
		Socket socket = new Socket(serverip, serversocket);
		OutputStream outputS = socket.getOutputStream();
		InputStream inputS = socket.getInputStream();
		BufferedReader bufferdIn = new BufferedReader(new InputStreamReader(inputS));
		
		String line;
		String enc = "";
		int counter = 0;
		int con = 0;
while(true) {
	counter = 0;
	String inp = in.nextLine();
	if(inp.equalsIgnoreCase("stop")) {
		break;
	}
	
	if(inp.equalsIgnoreCase("dir")) {
		outputS.write(("dir\n").getBytes());
		String out = "";
		String g = "";
		while((line = bufferdIn.readLine()) != null) {
			out = line;
			break;
		}
		String b = out.replace(" " , "");
		String[] ab = b.split(",");
		for (int i = 0; i < ab.length; i++) {
			g = g + " The file -------------> " + ab[i] + "\n";
			System.out.println(" The file -------------> " + ab[i] + "\n");
			//System.out.print(ab[i]);
		}
		
		stringtofileNoenc(g, "Dir.txt");
		System.out.println("Dir.txt in Main folder.");
	}
		if(inp.equalsIgnoreCase("accept")) {
			//System.out.println("1");
				String name = in.nextLine();
				String mid = Encryption.main(filetoString(name));
				outputS.write((inp + " " + name + " " + mid + "\n").getBytes());
				System.out.println("The File Was Sent");
		}
		
		if(inp.equalsIgnoreCase("get")) {
			String mod = in.nextLine();
			outputS.write(("get " + mod + "\n").getBytes());
			while((line = bufferdIn.readLine()) != null && counter == 0) {
				String[] cmd = line.split(" ");
				if(cmd[0].equalsIgnoreCase("Write")) {
					String mdio[] = cmd[1].split("==");
					con = packetcheck(mdio[0]);
					int aba = Integer.parseInt(mdio[1]);
					if(pecketch(con, aba)) {
						stringtofile(mdio[0], mod);
						System.out.println("The File Has Been Wirtten.");
					}else {
						outputS.write(("get " + mod + "\n").getBytes());
					}
				
				counter = 1;
				}
				if(counter == 1){
					break;
				}
			}
		}
	}
}
	public static void main(String[] args) {
		try {
			CLOONT(in.nextLine(), 42069);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void stringtofile(String tofile , String filename) {
		String file = "en\\" + filename;
		try {
			FileOutputStream out = new FileOutputStream(file);
			out.write(Base16Encoder.decode(Decryption.main(tofile)));
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void stringtofileNoenc(String tofile , String filename) {
		String file = filename;
		try {
			FileOutputStream out = new FileOutputStream(file);
			out.write(tofile.getBytes());
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String filetoStringnoenc(String file) {
		String input = "";
		String filename = "inputfile\\" + file;
		try {
			int i = 10000000;
			byte[] buffer = new byte[i];
			FileInputStream inputStream = new FileInputStream(filename);
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(filename));

			//  int total = 0;
			@SuppressWarnings("unused")
			int nRead = 0;

			i = inputStream.read(buffer);
			byte[] bufferr = new byte[i];
			//System.out.println(i);
			while((nRead = in.read(bufferr)) != -1) {

				//System.out.println(new String(buffer));
				//   total += nRead;
			} 

			input = new String(bufferr); 

			//input = new String(bufferr);
			//System.out.println(input);
		}catch (Exception e) {
			System.err.println(e);
		}
		return input;
	}

	public static String filetoString(String file) {
		String input = "";
		String filename = "inputfile\\" + file;
		try {
			int i = 10000000;
			byte[] buffer = new byte[i];
			FileInputStream inputStream = new FileInputStream(filename);
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(filename));

			//  int total = 0;
			@SuppressWarnings("unused")
			int nRead = 0;

			i = inputStream.read(buffer);
			byte[] bufferr = new byte[i];
			//System.out.println(i);
			while((nRead = in.read(bufferr)) != -1) {

				//System.out.println(new String(buffer));
				//   total += nRead;
			} 

			input = Base16Encoder.encode(bufferr); 

			//input = new String(bufferr);
			//System.out.println(input);
		}catch (Exception e) {
			System.err.println(e);
		}
		return input;
	}
	
	public static boolean pecketch(int main, int tocheck) {
		if(main == tocheck) {
			return true;
		}
		return false;
	}
	
	public static int packetcheck(String thefile) {
		int nob = 0;
		for (int i = 0; i < thefile.length(); i++) {
			nob++;
		}
		return nob;
	}
}
