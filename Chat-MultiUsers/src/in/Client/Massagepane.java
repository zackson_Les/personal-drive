package in.Client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import in.key.encryption.Decryption;
import in.key.encryption.Encryption;

public class Massagepane extends JPanel implements MassageListener{

	private Clinet client;
	private String login;

	private JTextField inputF = new JTextField();

	private DefaultListModel<String> ListModel = new DefaultListModel<>();
	private JList<String> MassageList = new JList<>(ListModel);


	public Massagepane(Clinet client, String login) {
		this.client = client;
		this.login = login;
		setLayout(new BorderLayout());
		add(new JScrollPane(MassageList) , BorderLayout.CENTER);
		add(inputF, BorderLayout.SOUTH);

		client.addMassageListener(this);

		inputF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String text = inputF.getText();
					String out = "";
					out = Encryption.main(text);
					client.msg(login, out);
					ListModel.addElement("You: " + text);
					inputF.setText("");
				} catch (IOException e1) {

					e1.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onMassege(String fromLogin, String msgBody) {
		if(login.equalsIgnoreCase(fromLogin)) {
			String main = Decryption.main(msgBody);
			String line = fromLogin + ": " + main;
			ListModel.addElement(line);
		}
	}
}
