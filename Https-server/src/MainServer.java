

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MainServer extends Thread{
	
	public static String outios = "";
	public static int workerscon = 0;
	private ArrayList<Server> workerslist = new ArrayList<>();
	private int serverport;
	public MainServer(int serverport) {
		this.serverport = serverport;
	}
	
	@Override
	public void run() {
		
		System.out.println("Waiting for Client Conection...");
		try {
			
			ServerSocket serversocket = new ServerSocket(serverport);
			while(true) {
				
				Socket clientsocket  = serversocket.accept();
				System.out.println(clientsocket.getInetAddress());
				Server thread = new Server(this ,clientsocket);
				workerslist.add(thread);
				thread.start();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
	}

}
	
	public List<Server> getworkerslist(){
		return workerslist;
	}
}
