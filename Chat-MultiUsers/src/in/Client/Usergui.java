package in.Client;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Usergui extends JPanel implements UserStatusListener{
	
	private final Clinet client;
	private JList<String> userListUI;
	private DefaultListModel<String> userListModel;
	public Usergui(Clinet client) {
		this.client = client;
		this.client.addUserStatusListener(this);
		
		userListModel = new DefaultListModel<>();
		userListUI = new JList<>(userListModel);
		setLayout(new BorderLayout());
		add(new JScrollPane(userListUI), BorderLayout.CENTER);
		userListUI.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() > 1) {
					String login = userListUI.getSelectedValue();
					
					Massagepane massagepane = new Massagepane(client, login);
					
					JFrame f = new JFrame();
					f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					f.setSize(600, 600);
					f.getContentPane().add(massagepane, BorderLayout.CENTER);
					f.setVisible(true);
				}
			}
		});
	}

	public static void main(String[] args) {
		Clinet client = new Clinet("localhost","550");
		Usergui usergui = new Usergui(client);
		
		JFrame frame = new JFrame("User List");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 600);
		frame.getContentPane().add(usergui, BorderLayout.CENTER);
		frame.setVisible(true);
		
		if(client.connect()) {
			client.Login();
		}
	}

	@Override
	public void offline(String login) {
		userListModel.removeElement(login);	
	}

	@Override
	public void online(String login) {
		userListModel.addElement(login);	
		
	}

}
