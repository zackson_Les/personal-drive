package drive.in.en;
import java.math.BigInteger;
public class base2 {
	  public static String main(byte[] a) throws Exception {
	    byte[] bytes = a;

	    // Create a BigInteger using the byte array
	    BigInteger bi = new BigInteger(bytes);

	    String s = bi.toString(2); 
	    return s;

	  }
	}
